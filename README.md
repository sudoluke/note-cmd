You can have a single file notebook using this pattern.

---SN---

some note content

more content with a @tag

I'm really taking notes now

---EN---

# note-cmd

Bash script to search plain text notes. 

Supports tags using "@".
## How to get started.
Clone the repo
```
git clone https://gitlab.com/sudoluke/note-cmd.git
```
Remove the test files if you want.
```
rm -rf test-dir/
```
Create the directory you want to use for your notes
```
mkdir <some_dir>
```
## OPTIONAL
Set the $NOTES_PATH environment variable with the path to your notes

edit ~/.bashrc and add the following
```
NOTES_PATH=~/some/path/
export NOTES_PATH
```
either close and reopen your terminal or `source ~/.bashrc` 
## Or if you just want to get started:
Just copy or link note-cmd.sh to where ever you keep your notes. `note-cmd.sh` will automatically detect the current working directory.
## Listing available tags
### List all tags
Example: list all tags in a note with `-lt`
```
./note-cmd.sh -lt
 The following notebooks contain these tags:
- ./test-dir/test2.txt
- ./test-dir/test.txt
###################################
Tags:
-----------------------------------
  10 @tag
   2 @spiderman_2021
   2 @spiderman
   2 @places
   2 @note
   2 @grand-canyon
   2 @fy_21-22
   2 @anothertag
   2 @2021
   2 @1600-pennsylvania
```
### List tags in a specific file
You can also get tags from individual files within the `NOTES_PATH`.
```
./note-cmd.sh -lt ./test-dir/test.txt
Tags in ./test-dir/test.txt
   5 @tag
   1 @anothertag
   1 @places
   1 @grand-canyon
   1 @fy_21-22
   1 @spiderman
   1 @2021
   1 @spiderman_2021
   1 @1600-pennsylvania
   1 @note
----
```
## Get a preview of tags
The `-t` or `--tag` option followed by a tag will print all lines in all files on the `$NOTE_PATH` that include that tag
See a preview of tags using `-t or --tag`
This option shows all files with the requested tag along with the full path from the `$NOTE_PATH` and the line number the tag appears on.
You can pipe this output to something like `less` if you have a lot of output.

`./note-cmd.sh -t @fy_21-22`
```
./test-dir
./test-dir/test2.txt:6:I visted the mars. In @fy_21-22
./test-dir/test.txt:8:I visted the @grand-canyon. In @fy_21-22
```
## Archive
You can create a hidden folder called `.archive` at your `$NOTEPATH` this directory will not be searched when listing tags or when displaying tag previews.
For instance if the `$NOTE_PATH` variable is set to `./notes` you can create `./notes/.archive` any notes in the archive will not be listed by the note-cmd.sh script.
