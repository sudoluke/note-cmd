#!/bin/bash
THIS="$0"
TAG="@"
RED="\033[31m"
GREEN='\033[0;32m'
NORMAL="\033[0;39m"
## Functions
get_note_path() {
## Get the NOTES_PATH if not defined.
if [ -z "$NOTES_PATH" ]
  then
    NOTES_PATH="$(dirname "$(readlink -f "${THIS}")")"
fi
echo $NOTES_PATH
}


### Show help
show_usage () {
  #Show Help
   echo "This script can search for and display tags from a plaintext note."
   echo
   echo "Syntax: note-cmd.sh [-h|-lt|-t]"
   echo "options:"
   echo "-lt or --list-tags         Show a list of all tags in a notebook."
   echo "-h                         Print this Help."
   echo "-t or --tag <@tag_name>    Display a specific tag."
   echo "-s or --show-notes <@tag_name>    Show all notes with a specific tag."
   echo
}

show_notes () {
files=$(find $(get_note_path) -path '*/.*' -prune -o -type f -print | grep -v note-cmd.sh 2>/dev/null)
awk -v search="$1" '/---SN---/{s="";p=0} {s=s $0 "\n"} $0~search{p=1} /---EN---/ && p{print s}' $files
}
### List tags
list_tags () {
files=$(find $(get_note_path) -path '*/.*' -prune -o -type f -print 2>/dev/null)
## IF -lt is passed with no file
## Need to check that filename is valid... maybe... or just let the shell handle it.
if [ -z "$1" ]
  then
#    echo -e $GREEN The following notebooks contain these tags: $NORMAL
#      for f in $files
#        do
#          echo -e  - $f 
#      done
  echo "###################################"
  tags=()
  echo "Tags:"
  echo "-----------------------------------"
  cat_file=$(cat $files 2> /dev/null)
  tr -cs '[@][[[:alnum:]-]_]' "\n" <<< "$cat_file" | grep -wo "[@][[:alnum:]].*" | sort | uniq -c | sort -nr
fi
## IF -lt is passed with a file name
## Need to check that filename is valid... maybe... or just let the shell handle it.
if [ ! -z $1 ]
  then
    echo "Tags in $1"
    tr -cs '[@][[[:alnum:]-]_]' "\n" < $1 | grep -wo "[@][[:alnum:]].*" | uniq -c
    echo "----"
fi
}
### Display lines with tags
show_tag () {
 #echo $NOTES_PATH
  grep --color=auto -Rwn --exclude-dir=".archive" $1 $NOTES_PATH 2> /dev/null
}
## Main
#NOTES_PATH="$(get_note_path)"
#echo $NOTES_PATH
if [[ ! $1  ]]
then
  show_usage
  exit 0
fi
while [ ! -z "$1" ];do
   case "$1" in
        -h|--help)
          show_usage
          exit 0
          ;;
        -lt|--list-tags)
          list_tags $2
          exit 0
          ;;
        -t|--tag)
          show_tag $2
          exit 0
          ;;
        -s|--show-notes)
          show_notes $2
          exit 0
          ;;
        *)
       echo -e $RED Incorrect options please read the help! $NORMAL
       show_usage
   esac
shift
done

