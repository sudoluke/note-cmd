---SN---
# First Note
This is a @tag
A @tag can be in the middle.
@tag can be in the front.
Some tags have puncuation after them, like @tag, or @anothertag.
Sometimes you might want to tag @places... 
I visted the @grand-canyon. In @fy_21-22
A Tag must start with the @ symbol and must be followed by a letter or a number. 
For example the following are valid tags: @spiderman is a valid tag. So is @2021 or @spiderman_2021, or @1600-pennsylvania.
The following are not valid tags: @_123, or @!tags
---
## Fancy title
This is a note section for @note1
If you separate notes into sections tag them @note, you'll see the whole section… someday… maybe…
Yay!
---
---EN---
---SN---
# Second Note
@note2
I visted @mars. In @fy_21-22
---EN---
---SN---
# Shopping list
@shopping-list
  - eggs
  - butter
  - cheese
  - stuff
---EN---
